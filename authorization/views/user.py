import logging

from authorization.models import User
from authorization.serializers.user import GetUserSerializer, UserSerializer

from rest_framework.viewsets import GenericViewSet
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework import mixins, status
from drf_spectacular.utils import extend_schema
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError

logger = logging.getLogger(__name__)


class RegisterViewSet(GenericViewSet, mixins.CreateModelMixin):
    serializer_class = UserSerializer
    permission_classes = [AllowAny]

    @extend_schema(
        request=serializer_class,
        responses={200: serializer_class},
        methods=['POST'],
        tags=['users']
    )
    def create(self, request, *args, **kwargs):
        try:
            email = request.data.get('email')
            logger.warning("STARTED REGISTER")
            return super().create(request, *args, **kwargs)
        except ValidationError as e:
            return Response({'detail': str(e)}, status=status.HTTP_400_BAD_REQUEST)


class GetUserViewSet(GenericViewSet, mixins.RetrieveModelMixin):
    queryset = User.objects.order_by('pk')
    permission_classes = [IsAuthenticated]
    serializer_class = GetUserSerializer

    def get_current_user(self, request, *args, **kwargs):
        user = request.user
        serializer = self.get_serializer(instance=user)
        return Response(status=status.HTTP_200_OK, data=serializer.data)
