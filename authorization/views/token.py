from rest_framework.permissions import AllowAny
from authorization.serializers.token import TokenObtainPairSerializer, TokenRefreshSerializer

from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView


class TokenObtainPairViewSet(TokenObtainPairView):
    permission_classes = [AllowAny]
    serializer_class = TokenObtainPairSerializer


class TokenRefreshViewSet(TokenRefreshView):
    permission_classes = [AllowAny]
    serializer_class = TokenRefreshSerializer
