from django.contrib.auth.hashers import make_password
from rest_framework import serializers

from authorization.models import User


class GetUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = (
            'id', 'last_name', 'first_name', 'email', 'phone_number'
        )


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(
        max_length=25,
        min_length=4,
        write_only=True
    )

    class Meta:
        model = User
        fields = (
            'id', 'email', 'last_name', 'first_name',
            'phone_number', 'password'
        )

    def save(self):
        password = self.validated_data.pop('password', None)
        if password:
            self.validated_data['password'] = make_password(password)

        return super().save()