import re

from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import PermissionsMixin

from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import gettext_lazy as _

from authorization.manage import UserManager


# Валидация номера телефона
def validate_phone_number(value):
    if not re.match(r"^\+?1?\d{11,15}$", value):
        raise ValidationError(
            'Номер телефона должен быть в формате +7XXXXXXXXXX'
        )


class User(AbstractBaseUser, PermissionsMixin):
    SUPERADMIN, ADMIN, BASIC = range(1, 4)
    objects = UserManager()

    ROLE_GROUP = {
        SUPERADMIN: 1,
        ADMIN: 2,
        BASIC: 3,
    }

    ROLE_TYPES = (
        (SUPERADMIN, _('SUPERADMIN')),
        (ADMIN, _('ADMIN')),
        (BASIC, _('BASIC')),
    )

    IS_ACTIVE = (
        (True, 'Не заблокирован'),
        (False, 'Заблокирован'),
    )

    id = models.AutoField(primary_key=True)
    email = models.EmailField(_('Почта'), max_length=50, unique=True)
    last_name = models.CharField("Фамилия", max_length=50, blank=True, null=True)
    first_name = models.CharField("Имя", max_length=50, blank=True, null=True)
    role = models.IntegerField(verbose_name=_('Роль'), default=BASIC, choices=ROLE_TYPES)
    phone_number = models.CharField(
        _('Номер телефона'), max_length=15, blank=True, null=True, validators=[validate_phone_number]
    )
    avatar = models.ImageField(
        upload_to='avatars/', verbose_name='Аватар',
        null=True, blank=True
    )
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(
        default=True,
        choices=IS_ACTIVE,
        verbose_name='Статус доступа',
    )
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _('Пользователь')
        verbose_name_plural = _('Пользователи')

    def __str__(self):
        return f'{self.last_name} {self.first_name}'

    def save(self, *args, **kwargs):
        if not self.password.startswith('pbkdf2_sha256'):
            self.password = make_password(self.password)
        super().save(*args, **kwargs)
