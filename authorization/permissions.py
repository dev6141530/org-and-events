from rest_framework.permissions import BasePermission
from authorization.models.user import User


class isSuperAdmin(BasePermission):
    def has_permission(self, request, view):
        return request.user.role == User.SUPERADMIN


class isAdmin(BasePermission):
    def has_permission(self, request, view):
        return request.user.role == User.ADMIN


class isBasic(BasePermission):
    def has_permission(self, request, view):
        return request.user.role == User.BASIC
