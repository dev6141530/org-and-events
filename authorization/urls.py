from django.urls import path, include
from authorization.views.token import TokenObtainPairView, TokenRefreshView
from authorization.views.user import GetUserViewSet, RegisterViewSet
from rest_framework import routers

router = routers.DefaultRouter()

urlpatterns = [
    path('', include(router.urls)),
    path('token/', TokenObtainPairView.as_view()),
    path('token/refresh/', TokenRefreshView.as_view()),
    path('get/', GetUserViewSet.as_view({'get': 'get_current_user'})),
    path('register/', RegisterViewSet.as_view({'post': 'create'})),
]